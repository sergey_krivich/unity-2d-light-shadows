// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/ShadowRenderer" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
				
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
#define PI 3.14
uniform sampler2D _MainTex;
uniform float4 _Resolution;
uniform float4 _Color;

struct v2f {
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

v2f vert( appdata_img v )
{
	v2f o;
	o.pos = UnityObjectToClipPos (v.vertex);
	o.uv = v.texcoord;
	return o;
}
	
//sample from the 1D distance map
float sam(float2 coord, float r) {
	return step(r, tex2D(_MainTex, coord).r);
}

float4 frag (v2f i) : SV_Target
{
	//rectangular to polar
	float2 norm = i.uv.xy * 2.0 - 1.0;
	float theta = atan2(norm.y, norm.x);
	float r = length(norm);
	//return float4(r, r, r, 1);

	float coord = (theta + PI) / (2.0*PI);

	//the tex coord to sample our 1D lookup texture	
	//always 0.0 on y axis
	float2 tc = float2(coord, 0.0);

	//the center tex coord, which gives us hard shadows
	float center = sam(tc, r);
	//return float4(center, center, center, 1);

	//we multiply the blur amount by our distance from center
	//this leads to more blurriness as the shadow "fades away"
	float blur = (1.0 / _Resolution.x)  * smoothstep(0.0, 1.0, r)*5;

	//now we use a simple gaussian blur
	float sum = 0.0;

	sum += sam(float2(tc.x - 4.0*blur, tc.y), r) * 0.05;
	sum += sam(float2(tc.x - 3.0*blur, tc.y), r) * 0.09;
	sum += sam(float2(tc.x - 2.0*blur, tc.y), r) * 0.12;
	sum += sam(float2(tc.x - 1.0*blur, tc.y), r) * 0.15;

	sum += center * 0.16;

	sum += sam(float2(tc.x + 1.0*blur, tc.y), r) * 0.15;
	sum += sam(float2(tc.x + 2.0*blur, tc.y), r) * 0.12;
	sum += sam(float2(tc.x + 3.0*blur, tc.y), r) * 0.09;
	sum += sam(float2(tc.x + 4.0*blur, tc.y), r) * 0.05;
	sum = clamp(sum, 0.5, 1);
	//sum of 1.0 -> in light, 0.0 -> in shadow

	//multiply the summed amount by our distance, which gives us a radial falloff
	//then multiply by vertex (light) color  
	return _Color * (sum * smoothstep(1.0, 0.0, r));
}
ENDCG

	}
}

Fallback off

}
