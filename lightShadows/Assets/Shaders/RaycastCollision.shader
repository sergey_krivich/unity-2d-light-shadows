// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/RaycastCollisions" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
				
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
#define PI 3.14
uniform sampler2D _MainTex;
uniform float4 _Resolution;

struct v2f {
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};

v2f vert( appdata_img v )
{
	v2f o;
	o.pos = UnityObjectToClipPos (v.vertex);
	o.uv = v.texcoord;
	return o;
}

const float THRESHOLD = 0.1;
float4 frag (v2f i) : SV_Target
{
  float distance = 1.0;
	//return float4(distance, 0, 0, 1.0);

  for (float y=0.0; y<_Resolution.y; y+=1.0) {
  		//rectangular to polar filter
		float2 norm = float2(i.uv.x, y/ _Resolution.y) * 2.0 - 1.0;
		float theta = PI*1.5 + norm.x * PI; 
		float r = (1.0 + norm.y) * 0.5;
		
		//coord which we will sample from occlude map
		float2 coord = float2(-r * sin(theta), -r * cos(theta))/2.0 + 0.5;

		//sample the occlusion map
		float4 data = tex2D(_MainTex, coord);
		
		//the current distance is how far from the top we've come
		float dst = y/ _Resolution.y;
		
		//if we've hit an opaque fragment (occluder), then get new distance
		//if the new distance is below the current, then we'll use that for our ray
		float caster = data.a;
		if (caster > THRESHOLD) {
			distance = min(distance, dst);
  		}
  }

  return float4(distance, distance, distance, 1.0);
}
ENDCG

	}
}

Fallback off

}
