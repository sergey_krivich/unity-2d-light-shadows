﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowCaster : MonoBehaviour {

	void Start () {

    var shadowRender = new GameObject("Shadow");
    shadowRender.transform.SetParent(transform);
	  shadowRender.layer = LayerMask.NameToLayer("Shadow");
	  var sr = shadowRender.AddComponent<SpriteRenderer>();
	  var mr = GetComponent<SpriteRenderer>();

    sr.sprite = mr.sprite;
	  sr.drawMode = mr.drawMode;
	  sr.size = mr.size;
    shadowRender.transform.localPosition = new Vector3();
	  sr.tileMode = mr.tileMode;

    sr.color = new Color(0, 0, 0, 1);
	}
	
	void Update () {
		
	}
}
