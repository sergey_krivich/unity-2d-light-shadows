﻿using UnityEngine;

public class Light2D : MonoBehaviour
{
  public Camera RenderCamera;
  public Material CollisionMaterial;
  public Material ShadowRenderMaterial;

  public RenderTexture _texture;
  public RenderTexture _visibilityMap;
  public RenderTexture _shadowRender;

  public MeshRenderer _renderer;

  public Color LightColor = Color.green;
  public float Radius = 15f;
  public float PixelsPerRadius = 50;
  public float OrthosizePerRadius = 0.25f;

	void Start ()
	{
	  var size = (int)(Radius * PixelsPerRadius);

    _texture = new RenderTexture(size, size, 32, RenderTextureFormat.ARGB32);
    _shadowRender = new RenderTexture(size, size, 32, RenderTextureFormat.ARGB32);
	  _visibilityMap = new RenderTexture(size/4, 1, 32, RenderTextureFormat.ARGB32);
	  RenderCamera.targetTexture = _texture;

    _renderer.material.mainTexture = _shadowRender;
  }

  void LateUpdate ()
  {
    RenderCamera.orthographicSize = Radius * OrthosizePerRadius;
    _renderer.transform.localScale = new Vector3(Radius, -Radius, Radius)*0.5f;
    RenderCamera.targetTexture = _texture;
    RenderCamera.Render();
    CollisionMaterial.SetVector("_Resolution", new Vector4(_texture.width, _texture.height, 0, 0));
    Graphics.Blit(_texture, _visibilityMap, CollisionMaterial);
    RenderCamera.targetTexture = null;

    ShadowRenderMaterial.SetVector("_Resolution", new Vector4(_shadowRender.width, _shadowRender.height, 0, 0));
    ShadowRenderMaterial.SetColor("_Color", LightColor);
    Graphics.Blit(_visibilityMap, _shadowRender, ShadowRenderMaterial);
  }
}
