﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float Speed = 5;
    public Transform Target;

    private void Start()
    {
        I = this;
    }

    private void Update()
    {
        if (Target == null)
        {
            return;
        }

        Vector3 curPos = transform.position;
        Vector3 targetPos = Target.position + new Vector3(0, 0, -10);

        transform.position = Vector3.Lerp(curPos, targetPos, Speed*Time.deltaTime);
    }

    public static CameraControl I { get; private set; }
}
