﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// this script just captures the OnTrigger* messages and passes them on to the CharacterController2D
/// </summary>
public class CC2DTriggerHelper : MonoBehaviour
{
	private CharacterController2D _parentCharacterController;
    private List<Collider2D> _colliders = new List<Collider2D>();

	public void setParentCharacterController( CharacterController2D parentCharacterController )
	{
		_parentCharacterController = parentCharacterController;
	}


	#region MonoBehavior

	void OnTriggerEnter2D( Collider2D col )
	{
	    if (col.isTrigger)
	    {
	        _parentCharacterController.OnTriggerEnter2D(col);
            _colliders.Add(col);
        }
	}

    void Update()
    {
        _colliders.RemoveAll(p => p == null);
        foreach (var d in _colliders)
        {
            _parentCharacterController.OnTriggerStay2D(d);
        }
    }

	void OnTriggerExit2D( Collider2D col )
	{
	    if (col.isTrigger)
	    {
	        _parentCharacterController.OnTriggerExit2D(col);
            _colliders.Remove(col);
        }
    }

	#endregion

}
